// Tests related to src/components/App.vue

module.exports = {
  before: function(browser) {
    const endpoint = browser.globals.devServerURL;

    browser.url(endpoint);
    browser.waitForElementVisible('#list', 10000);
  },

  after: function(browser) {
    browser.end();
  },

  'Evaluating presence of .features div': function test(browser) {
    browser.waitForElementVisible('#features', 2000);
  },


  'Evaluating content of .search-title element': function test(browser) {
    browser.expect.element('#search-title').to.be.an('input');
    browser.assert.attributeEquals('#search-title', 'type', 'text');
    browser.assert.attributeEquals('#search-title', 'placeholder', 'What title are you looking for?');
    browser.assert.containsText('label[for=search-title]', 'Search');
  },

  'Evaluating content of .order-by-date element': function test(browser) {
    browser.expect.element('#order-by-date').to.be.an('input');
    browser.assert.attributeEquals('#order-by-date', 'type', 'checkbox');
    browser.assert.containsText('label[for=order-by-date]', 'Order by date?');
  },

  'Evaluating result message after empty search': function test(browser) {
    browser.setValue('input[id=search-title]', 'a text that will return an empty search for sure!!!');
    browser.assert.containsText('span', 'Sorry, there is no Prezi with this title... :(');
  },
};
