// Tests related to src/components/App.vue

module.exports = {
  before: function(browser) {
    const endpoint = browser.globals.devServerURL;

    browser.url(endpoint);
    browser.waitForElementVisible('#list', 10000);
  },

  after: function(browser) {
    browser.end();
  },

  'Evaluating presence of at least one .thumbnail div': function test(browser) {
    browser.waitForElementVisible('#thumbnail', 2000);
  },

  'Evaluating content of .thumbnail div': function test(browser) {
    browser.expect.element('#thumbnail-img').to.be.an('img');
    browser.assert.attributeEquals('#thumbnail-img', 'width', '125');
    browser.assert.attributeEquals('#thumbnail-img', 'class', 'z-depth-3');
    browser.assert.containsText('label[for=search-title]', 'Search');
  },

  'Evaluating presence of at least one .info div': function test(browser) {
    browser.waitForElementVisible('#info', 2000);
  },

  'Evaluating content of .info div': function test(browser) {
    browser.expect.element('#info-uid').to.be.a('span');
    browser.assert.containsText('label[for=info-uid]', 'UID');

    browser.expect.element('#info-title').to.be.a('span');
    browser.assert.containsText('label[for=info-title]', 'TITLE');

    browser.expect.element('#info-created-at').to.be.a('span');
    browser.assert.containsText('label[for=info-created-at]', 'CREATED AT');

    browser.expect.element('#info-creator-profile').to.be.an('img');
  },
};
