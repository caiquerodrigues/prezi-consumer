// Tests related to src/components/App.vue

module.exports = {
  before: function(browser) {
    const endpoint = browser.globals.devServerURL;

    browser.url(endpoint);
    browser.waitForElementVisible('#app', 5000);
  },

  after: function(browser) {
    browser.end();
  },

  'Evaluating content of .title element': function test(browser) {
    browser.expect.element('#title').to.be.a('h1');
    browser.expect.element('#title').text.to.equal('Prezi Consumer');
  },

  'Evaluating content of .subtitle-1 element': function test(browser) {
    browser.expect.element('#subtitle-1').to.be.a('h5');
    browser.expect.element('#subtitle-1').text.to.equal('Here you can find all your next level presentations.');
  },

  'Evaluating content of .subtitle-2 element': function test(browser) {
    browser.expect.element('#subtitle-2').to.be.a('h5');
    browser.expect.element('#subtitle-2').text.to.equal('We are not talking about the boring and static files but the really awesome ones!');
  },
};
