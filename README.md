# Prezi Home Assignments - Consumer

## TODO (things which could be done to make this even better)
#### Backend Extras
- Define a seed file or initializer to execute the insertions into the DB when the application starts
- Create a dynamic route allowing custom fetching with parameters like:
  - author
  - limit of entries retrieved
  - single and random entry

#### Frontend Extras
- Develop unit tests for JS files
- Improve e2e with Page Objects pattern
- Improve the title search with a better algorithm (e.g:. fuzzy search)
- Add other features with VueJS, such as:
  - carrousel component showing data samples
  - modal component showing author's details
  - add search by author
- Create a static route displaying useful infos

#### infrastructure/DevOps Extras
- Build a Docker image for Postgres in order to make development environment similar to production
- Build a Docker environment with both projects and all dependencies for a better deploy and development

## Important Things (ALWAYS WORRY ABOUT THESE THINGS)
While I develop any application, complex or simple, I believe the code organization (starting with application structure and going further until the code itself) is essential because it enables you to understand, evolve, maintain and scale your product as needed. Provide a clean code is also essential and a good code can turns into your best documentation.

From the point of view related to architecture I prefer to develop unplugged and modular applications just like this. I believe in API's power and also that is easier to maintain an independent frontend application. For example, the most obvious advantage is that you can create multiple clients in order to improve user experience using the same business logic and have specialized teams working independently.

I'm also worried about use a clean, easy to build, and maintainable environment, which improves the team's velocity by reducing problems with individual environments. My actual team practices DevOps and we use Docker in each of our local environments. Today we do not deploy only our code, we deploy our complete infrastructure in staging and production.

Dedicate a good time to develop high quality tests(from unit to e2e) is essential especially when DevOps is inside the team's culture. This ensures that everything is working properly and you become confident to deploy, evolve, or change your code.

In conclusion, I believe that good software products are built by delivering high quality products at all levels. A good software developer must understand about other important stuff that goes beyond good code (which is more than essential). Good code is important but the magic happens when we group all the software components together building a good development process as a whole.

## Description
This is a Padrino project developed to provide an API for the business logics related to the home assignment proposed for Prezi.

## Tasks
##### Your task is to create a webpage and a backend service with which you can:
- [OK] see all the presentations with their data (thumbnail, title, creator, creation date)
- [OK] you can search by title (a really dumb search is OK)
- [OK] order by creation date
- [OK] the data should be served by a backend service. It's not enough to only use the json file.
- [OK] the frontend should be a separate static site
- [OK] There's a json file attached. Use it as a data source in the backend service.

##### The output:
- [OK] github/bitbucket repo(s)
- [OK] a few words about how you'd make it even more awesome if you were given more time.

##### Extra points:
- [OK] proper commit history (vs. a single commit when you're done)
- [OK] the service and the webpage deployed somewhere (e.g. Heroku)

## HOW-TOs
#### Configuring `prezi-presenter` API
This project depends entirely of `prezi-presenter`. Please access the Padrino API project for further instructions about how to put it up and running. Come back here after this, follow one of the guides below, and then access [http://localhost:5000/](http://localhost:5000/).

Maybe you will see `Sorry, there is no Prezi with this title... :(` message but you just have to wait and the API will be fetched updating the Prezis list. This delay depends on your connection and other hardware definitions.

#### Enjoying Docker
You can easily ship a container and up this project taking advantage of the `Dockerfile` located in the root folder of this project. Please install [Docker](https://www.docker.com/) and run the following commands from the root folder of this project.

Be sure that port `5000` is available before proceed! Enjoy!

```
docker build -t prezi-consumer .
docker run -d --name prezi-consumer -p 5000:8080 prezi-consumer npm run dev
```

After this you can use `docker stop prezi-consumer` and `docker start prezi-consumer` to manage your container. Also use `docker ps`to check the containers' status.

When your container is up and running you can access your application through [http://localhost:5000/](http://localhost:5000/).

#### Being classic
If you don't want to use Docker and prefer to keep yourself with the static environment, please follow the commands below from the root folder of this project. Enjoy!

```
npm install
npm run dev
```

When your Node server is up and running you can access your application through [http://localhost:8080/](http://localhost:8080/).

Be sure that port `8080` is available before proceed.

#### Defining an AMAZING host
- Open your `/etc/hosts`. If you don't have this file maybe it is time to think about your life, all the things you've done, and also...**WHY ARE YOU USING WINDOWS!?**
- Put at the end of file the following line:

```
127.0.0.1 prezi-services.local
```

#### Running e2e Nightwatch Specs
- You just have to run `npm install` and then `npm run e2e` to execute all tests located at `test/e2e/specs`.

#### Deploying to Heroku
- Add remote referencing Heroku to this repository. I high recommend [heroku-cli](https://devcenter.heroku.com/articles/heroku-cli)
- Change the code and commit everything
- Run `npm run build` to generate a new `dist` folder and then commit the changes
- Push your modifications with `git push heroku master` or `git push heroku other_branch:master`
- Run `heroku open` and check the result
