/* eslint-disable no-shadow, no-param-reassign */
import provider from '../../api/provider';
import * as types from '../mutation-types';

// initial state
const state = {
  all: [],
};

// getters
const getters = {
  allPrezis: state => state.all,
};

// actions
const actions = {
  getAllPrezis({ commit }) {
    provider.getRealPrezis().then((response) => {
      const prezis = response.data;
      commit(types.RECEIVE_PREZIS, { prezis });
    });
  },
};

// mutations
const mutations = {
  [types.RECEIVE_PREZIS](state, { prezis }) {
    state.all = prezis;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
/* eslint-enable */
