import Vue from 'vue';
import Vuex from 'vuex';

import prezi from './modules/prezis';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    prezi,
  },
});
