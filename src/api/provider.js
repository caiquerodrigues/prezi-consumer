import axios from 'axios';

/**
 * Mocking client-server processing
 */
const prezis = [
  { uid: 1, title: 'Great Prezi 1', date: '2013-03-06' },
  { uid: 2, title: 'Great Prezi 2', date: '2011-03-06' },
  { uid: 3, title: 'Great Prezi 3', date: '2012-03-06' },
];

const ENDPOINT = process.env.ENDPOINT;

const AXIOS_INSTANCE = axios.create({ baseURL: ENDPOINT });

export default {
  getPrezis(cb) {
    setTimeout(() => cb(prezis), 100);
  },
  getRealPrezis() {
    return AXIOS_INSTANCE.get('/all');
  },
};
