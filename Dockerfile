FROM node:boron

ENV NODE_ENV development

RUN apt-get update && \
    apt-get install -qy build-essential libpq-dev npm nodejs --no-install-recommends && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir /app

WORKDIR /app

COPY ./ /app

RUN npm install

EXPOSE 8080
